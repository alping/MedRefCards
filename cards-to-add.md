Vuxna och barn på olika kort

## Urakut, typ 2 kort
- Sepsis
- Anafylaxi
- Hjärtstopp

## Medicin
- Spirometri

- EKG1
- EKG2

- Hjärt-EKO
- Hjärtauskultation, blåsljud

- Hormonaxlar

- Elektrolytrubbningar

- HLR, schema + detaljer joul mm
- HLR, 4H, 4T

- Radiologimodaliteter
- Radiologibedömning

- Wells score

- HUSK-MIDAS

## Hud
- Efflorescenser
- Vanliga huddiagnoser

- Resektionsgränser

## Kirurgi
- Chirurgia Minor
    + Suturmaterial
    + Suturteknik

- Blodgas
    + Diagnosförslag
- Vätsketerapi
    + Ringer
    + Glukos
    + Blod
    + Hypo/Hypernatremi

- Basalbehov

- EKG

- Hjärtinfarkt

- Sepsiskriterier: QSOFA

- Antibiotika
- Sepsisbehandlingar?
- Antibiotikaklasser och patogener de slår på

## Ortopedi
- CSPINE

## Farmakologi
- Generella koncept, steady-state, halveringstid
- Vanliga doser

## Psykiatri
- Psykstatus
- Suicidstege

- LPT lagstiftningen

- Depression, diagnos
- Depression, behandling

## OBGYN
- Menstrationsrubbningar
- Ovulationsgraf

## Ögon
- Flowcharts (det röda ögat)